ORM Schemas
===========

Plugin for Node ORM2 postgres backend. Allowing For schema switching prior to execting any normal operation with ORM2

    var orm require('orm');
    var schemas = require('orm-schemas');
    var db = orm.connect({
    	...
    }, function( err, db ){
    	if( err ){
    		console.log( err.message );
    		process.exit( 0 );
    	}

    	db.use( schemas );
    	db.set_schema("alternamte_schema", function( err, trans ){
    		try{
	    		db.execQuery("SELECT * FROM foobar", function( err, datas ){
		    		trans.commit();
				});
    		} catch( e ) {
    			trans.rollback();
    		}
		})
	});
