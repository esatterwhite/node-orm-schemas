/**
 * Provides ablity to switch the search path for the PostgreSQL backend with transaction support
 * @module plugin
 * @author Eric Satterwhite
 * @requires debug
 * @requires util
 **/
var debug = require("debug")("orm/schema")
	,util = require("util")
	,Plugin;


/**
 * Primary Plugin. Adds set_schema method do the orm
 * @param {ORM} db An instance of the node-orm
 */
Plugin = function( db ){
	debug("loading ORM Schemas Plugin")

	var transaction = {
		commit: function commit(callback) {
			db.driver.execQuery("COMMIT", function (err) {
				debug("transaction committed!");
				return (callback||function(){})(err || null);
			});
		},
		rollback: function rollback(callback) {
			db.driver.execQuery("ROLLBACK", function (err) {
				debug("transaction rolled back!");
				return callback(err || null);
			});
		}
	};

	/**
	 * sets the schema search path inside of a transaction
	 * @param {String} schema The schema name to set as the search path
	 * @param {Function} callback the to execute upon completion. will be passes an error if there is one and a transaction object.
	 * You must call either `commit` or `rollback` on the transaction to complete the query.
	 * @return {Mixed} returns the result of the `callback` funtion
	 **/
	db.set_schema = function( schema, cb ){
		var query = util.format( "BEGIN; SET search_path TO %s;", schema );

		db.driver.execQuery(query , function callback( err ){
			if( err ){
				return cb( err );
			}
			debug("search path set to "  + schema );

			return cb(null, transaction);
		});

	};

	return {
		define: function( Model ){
			Model.settings.set( "public.schema", true )

			Object.defineProperty(Model,"search_path",{
				get: function(){}
			})

		}
	};
}

// The module is the plugin
module.exports = Plugin;
